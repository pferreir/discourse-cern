FROM ruby:2.4.2
MAINTAINER web-services-core <web-services-core@cern.ch>

### Install prerequisites #############################
RUN apt update && \
    apt install -y lsb-release wget gettext gifsicle jpegoptim optipng jhead && \
    echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list && \
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    apt-get update && apt-get -y upgrade && \
    apt-get -y install postgresql-client-9.6

### Set up Discourse ##################################
#   - So we can overrride it with --build_arg
ARG DISCOURSE_VERS='v1.8.10'
ENV DISCOURSE_RELEASE=$DISCOURSE_VERS RAILS_ENV=production RAILS_ROOT=/discourse HOME=/discourse

RUN mkdir -p discourse && \
    git clone --depth=1 --single-branch --branch "$DISCOURSE_RELEASE"  https://github.com/discourse/discourse.git /discourse

WORKDIR $HOME

RUN mkdir -p ./tmp/pids/ && \
    ### Plugins
    #   - OAuth
    git clone --depth=1 https://github.com/discourse/discourse-oauth2-basic.git /discourse/plugins/discourse-oauth2-basic && \
    #   - Chat Integration
    #git clone --depth=1 https://github.com/discourse/discourse-chat-integration.git /discourse/plugins/discourse-chat-integration && \
    ### Gem installation
    exec bundle install --deployment --without development:test && \
    exec bundle clean -V

ADD ["cgroup-limits","run-discourse.sh","run-sidekiq.sh","./"]
RUN chmod +x ./run-discourse.sh ./run-sidekiq.sh && \
    chmod -R a+rw /discourse

### entrypoint will be overriden for the sidekiq container
ENTRYPOINT ["./run-discourse.sh"]

EXPOSE 8080