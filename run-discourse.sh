#!/bin/bash

# If there are files in /tmp/configmap that are not empty
# (overriden by a ConfigMap) copy them to /discourse/config
if [ -n "$(ls -A /tmp/discourse-configmap)" ]
then
  for f in /tmp/discourse-configmap/*
  do
    if [ -s $f ]
    then
      cp /tmp/discourse-configmap/* /discourse/config/
    fi
  done
fi

# Replace environment variables
echo "--> Overwritting env variables ..."
envsubst < /tmp/discourse-configmap/discourse.conf > /discourse/config/discourse.conf
echo "--> DONE"

# Set environment as production
export RAILS_ENV="production"

# Create or Migrate DB
bundle exec rake db:create db:migrate
# Always precompile assets
bundle exec rake assets:precompile

# Puma is installed, so get memory information
export_vars=$(python cgroup-limits) ; export $export_vars

# Run discourse with Puma
exec bundle exec puma -C ./config/puma.rb config.ru