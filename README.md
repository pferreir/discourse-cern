# Openshift Template for Discourse

This template will create the basic structure of an OpenShift service running Discourse. Any assets you put under the
`images/` dir will be copied to [`/discourse/public/images/`](https://github.com/discourse/discourse/tree/master/public/images).

## Prerequisites

Postgresql database with two extensions:
- `hstore`
- `pg_trgm`

Create a PaaS Application
- https://webservices.web.cern.ch/webservices/Services/CreateNewSite/Default.aspx
- Set quota to medium (recommended) https://cern.service-now.com/service-portal/article.do?n=KB0004427

## Usage
Example usage:

```sh
$ cd templates
$ ./config_project.sh HOSTNAME=mynewforum.web.cern.ch DISCOURSE_DB_HOST=some-dbod-server.cern.ch DISCOURSE_DB_PORT=6603 \
DISCOURSE_DB_PASSWORD=xxx DISCOURSE_DEVELOPER_EMAILS=alice@cern.ch,bob@cern.ch
```

OAuth2 config parameters (Admin > Settings > Login):

```
oauth2_authorize_url = https://oauth.web.cern.ch/OAuth/Authorize
oauth2_token_url = https://oauth.web.cern.ch/OAuth/Token
oauth2_user_json_url = https://oauthresource.web.cern.ch/api/User?oauth_token=:token
oauth2_json_user_id_path = personid
oauth2_json_username_path = username
oauth2_json_name_path = name
oauth2_json_email_path = email
```