apiVersion: v1
kind: Template
metadata:
  name: discourse-cern
  annotations:
    description: "Discourse for CERN template"
labels:
  template: "discourse-cern" #this label will applied to all objects created from this template
objects:
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      app: discourse-cern
    name: webapp
  spec:
    ports:
    - name: 8080-tcp
      port: 8080
      protocol: TCP
      targetPort: 8080
    selector:
      app: discourse-cern
      deploymentconfig: webapp
    sessionAffinity: None
    type: ClusterIP

- apiVersion: v1
  kind: Service
  metadata:
    labels:
      app: discourse-cern
    name: redis
  spec:
    ports:
    - name: 6379-tcp
      port: 6379
      protocol: TCP
      targetPort: 6379
    selector:
      app: discourse-cern
      deploymentconfig: redis
    sessionAffinity: None
    type: ClusterIP

- apiVersion: v1
  kind: Route
  metadata:
    labels:
      app: discourse-cern
    name: webapp
  spec:
    port:
      targetPort: 8080-tcp
    to:
      kind: Service
      name: webapp
      weight: 100
    tls:
      termination: "edge"
      insecureEdgeTerminationPolicy: Redirect

- kind: DeploymentConfig
  apiVersion: v1
  metadata:
    labels:
      app: discourse-cern
    name: webapp
  spec:
    replicas: 1
    selector:
      app: discourse-cern
      deploymentconfig: webapp
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          app: discourse-cern
          deploymentconfig: webapp
      spec:
        containers:
        -
          name: webapp
          image: gitlab-registry.cern.ch/webservices/discourse-cern:stable
          imagePullPolicy: IfNotPresent
          ports:
          - containerPort: 8080
            protocol: TCP
          resources:
            limits:
              # limit as per https://github.com/discourse/discourse/blob/master/docs/ADMIN-QUICK-START-GUIDE.md#maintenance
              memory: 1Gi
              cpu: 1
            requests:
              memory: 320Mi
              cpu: 200m
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - mountPath: /tmp/discourse-configmap
            name: discourse-configmap
          - mountPath: /discourse/public/uploads
            name: discourse-uploads
          readinessProbe:
            failureThreshold: 3
            initialDelaySeconds: 30
            periodSeconds: 10
            successThreshold: 1
            tcpSocket:
              port: 8080
            timeoutSeconds: 10
          livenessProbe:
            failureThreshold: 3
            initialDelaySeconds: 300
            periodSeconds: 10
            successThreshold: 1
            tcpSocket:
              port: 8080
            timeoutSeconds: 10
          env:
              - name: "NAMESPACE"
                valueFrom:
                  fieldRef:
                    apiVersion: v1
                    fieldPath: metadata.namespace

              - name: HOSTNAME
                value: "$(NAMESPACE).web.cern.ch"

              - name: DISCOURSE_DB_HOST
                value: "${DISCOURSE_DB_HOST}"

              - name: DISCOURSE_DB_PORT
                value: "${DISCOURSE_DB_PORT}"

              - name: DISCOURSE_DB_NAME
                value: "${DISCOURSE_DB_NAME}"

              - name: DISCOURSE_DB_USERNAME
                value: "${DISCOURSE_DB_USERNAME}"

              - name: DISCOURSE_DB_PASSWORD
                value: "${DISCOURSE_DB_PASSWORD}"

              - name: DISCOURSE_DEVELOPER_EMAILS
                value: "${DISCOURSE_DEVELOPER_EMAILS}"
                
              - name: PUMA_WORKERS
                value: '1'

        dnsPolicy: ClusterFirst
        restartPolicy: Always
        securityContext:
            capabilities: {}
            privileged: false

        volumes:
        - name: discourse-configmap
          configMap:
            name: discourse-configmap
        - name: discourse-public-assets
          emptyDir: {}
        - name: discourse-uploads
          persistentVolumeClaim:
            claimName: discourse-uploads
    triggers:
    - type: ConfigChange
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - "webapp"
        from:
          kind: ImageStreamTag
          name: discourse-cern:stable
          namespace: openshift

- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: discourse-uploads
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: 5Gi
  status: {}

- kind: DeploymentConfig
  apiVersion: v1
  metadata:
    labels:
      app: discourse-cern
    name: sidekiq
  spec:
    replicas: 1
    selector:
      app: discourse
      deploymentconfig: sidekiq
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          app: discourse
          deploymentconfig: sidekiq
      spec:
        containers:
        -
          name: sidekiq
          image: gitlab-registry.cern.ch/webservices/discourse-cern:stable
          imagePullPolicy: IfNotPresent
          command: # sidekiq container has a different entrypoint
              - ./run-sidekiq.sh
          ports:
          - containerPort: 8080
            protocol: TCP
          resources:
            limits:
              memory: 800Mi
              cpu: "1"
            requests:
              memory: 400Mi
              cpu: 200m
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - mountPath: /tmp/discourse-configmap
            name: discourse-configmap
          - mountPath: /discourse/public/uploads
            name: discourse-uploads
          readinessProbe:
            exec:
              command:
                - cat
                - /discourse/tmp/pids/sidekiq.pid
            failureThreshold: 3
            initialDelaySeconds: 60
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 10
          livenessProbe:
            exec:
              command:
                - cat
                - /discourse/tmp/pids/sidekiq.pid
            failureThreshold: 3
            initialDelaySeconds: 300
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 10
          env:
              - name: "NAMESPACE"
                valueFrom:
                  fieldRef:
                    apiVersion: v1
                    fieldPath: metadata.namespace
              - name: HOSTNAME
                value: "$(NAMESPACE).web.cern.ch"

        dnsPolicy: ClusterFirst
        restartPolicy: Always
        securityContext:
            capabilities: {}
            privileged: false
        volumes:
        - name: discourse-configmap
          configMap:
            name: discourse-configmap
        - name: discourse-uploads
          persistentVolumeClaim:
            claimName: discourse-uploads
    triggers:
    - type: "ConfigChange"
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - "sidekiq"
        from:
          kind: "ImageStreamTag"
          name: "discourse-cern:stable"
          namespace: openshift

- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      app: discourse-cern
    name: redis
  spec:
    replicas: 1
    selector:
      app: discourse-cern
      deploymentconfig: redis
    strategy:
      type: Rolling
    template:
      metadata:
        labels:
          app: discourse-cern
          deploymentconfig: redis
      spec:
        containers:
        - image: '172.30.78.46:5000/openshift/redis:3.2'
          imagePullPolicy: Always
          name: redis
          ports:
          - containerPort: 6379
            protocol: TCP
          resources: {}
          terminationMessagePath: /dev/termination-log
          volumeMounts:
          - mountPath: /var/lib/redis/data
            name: redis-1
          readinessProbe:
            failureThreshold: 3
            initialDelaySeconds: 5
            periodSeconds: 10
            successThreshold: 1
            tcpSocket:
              port: 6379
            timeoutSeconds: 10
          livenessProbe:
            failureThreshold: 3
            initialDelaySeconds: 30
            periodSeconds: 10
            successThreshold: 1
            tcpSocket:
              port: 6379
            timeoutSeconds: 10
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        securityContext: {}
        volumes:
        - emptyDir: {}
          name: redis-1
    test: false
    triggers:
    - type: ConfigChange

- kind: "ConfigMap"
  apiVersion: v1
  metadata:
    name: "discourse-configmap"
  data:
    discourse.conf: |2
      # connection pool size, sidekiq is set to 5, allowing an extra 3 for bg threads
      db_pool = 8

      # database timeout in milliseconds
      db_timeout = 5000

      # socket file used to access db
      db_socket =

      # host address for db server
      # This is set to blank so it tries to use sockets first
      db_host = ${DISCOURSE_DB_HOST}

      # port running db server, no need to set it
      db_port = ${DISCOURSE_DB_PORT}

      # database name running discourse
      db_name = ${DISCOURSE_DB_NAME}

      # username accessing database
      db_username = ${DISCOURSE_DB_USERNAME}

      # password used to access the db
      db_password = ${DISCOURSE_DB_PASSWORD}

      # Disallow prepared statements
      # see: https://github.com/rails/rails/issues/21992
      db_prepared_statements = false

      # host address for db replica server
      db_replica_host =

      # port running replica db server, defaults to 5432 if not set
      db_replica_port =

      # hostname running the forum
      hostname = ${HOSTNAME}

      # backup hostname mainly for cdn use
      backup_hostname =

      # address of smtp server used to send emails
      smtp_address = 'cernmx.cern.ch'

      # port of smtp server used to send emails
      smtp_port = '25'

      # domain passed to smtp server
      smtp_domain = 'cern.ch'

      # username for smtp server
      smtp_user_name =

      # password for smtp server
      smtp_password =

      # smtp authentication mechanism
      smtp_authentication = 'none'

      # enable TLS encryption for smtp connections
      smtp_enable_start_tls = false

      # mode for verifying smtp server certificates
      # to disable, set to 'none'
      smtp_openssl_verify_mode = 'none'

      # load MiniProfiler in production, to be used by developers
      load_mini_profiler = true

      # recommended, cdn used to access assets
      cdn_url =

      # comma delimited list of emails that have developer level access
      developer_emails = ${DISCOURSE_DEVELOPER_EMAILS}

      # redis server address
      redis_host = ${REDIS_PORT_6379_TCP_ADDR}

      # redis server port
      redis_port = ${REDIS_PORT_6379_TCP_PORT}

      # redis slave server address
      redis_slave_host =

      # redis slave server port
      redis_slave_port = ${REDIS_PORT_6379_TCP_PORT}

      # redis database
      redis_db = 0

      # redis password
      redis_password =

      # redis sentinels eg
      # redis_sentinels = 10.0.0.1:26381,10.0.0.2:26381
      redis_sentinels =

      # enable Cross-origin Resource Sharing (CORS) directly at the application level
      enable_cors = true
      cors_origin = '*.cern.ch'

      # enable if you really need to serve assets in prd
      serve_static_assets = true

      # number of sidekiq workers (launched via unicorn master)
      sidekiq_workers = 5

      # adjust stylesheets to rtl (requires "rtlit" gem)
      rtl_css = false

      # notify admin when a new version of discourse is released
      # this is global so it is easier to set in multisites
      # TODO allow for global overrides
      new_version_emails = true

      # connection reaping helps keep connection counts down, postgres
      # will not work properly with huge numbers of open connections
      # reap connections from pool that are older than 30 seconds
      connection_reaper_age = 30
      # run reap check every 30 seconds
      connection_reaper_interval = 30
      # also reap any connections older than this
      connection_reaper_max_age = 600

      # set to relative URL (for subdirectory hosting)
      # IMPORTANT: path must not include a trailing /
      # EG: /forum
      relative_url_root =

      # increasing this number will increase redis memory use
      # this ensures backlog (ability of channels to catch up are capped)
      # message bus default cap is 1000, we are winding it down to 100
      message_bus_max_backlog_size = 100

      # must be a 64 byte hex string, anything else will be ignored with a warning
      secret_key_base =

      # fallback path for all assets which are served via the application
      # used by static_controller
      # in multi host setups this allows you to have old unicorn instances serve
      # newly compiled assets
      fallback_assets_path =


    puma.rb: |2
      def get_max_memory()
          return ENV['MEMORY_LIMIT_IN_BYTES'].to_i if ENV.has_key? 'MEMORY_LIMIT_IN_BYTES'

          # Assume unlimited memory. 0.size is the number of bytes a Ruby
          # Fixnum class can hold. One bit is used for sign and one is used
          # by Ruby to determine whether it's a number or pointer to an object.
          # That's why we subtract two bits. This expresion should therefore be
          # the largest signed Fixnum possible.
          (2 ** (8*0.size - 2) - 1)
      end

      def get_memory_per_worker()
          bytes = ENV.fetch('MEMORY_BYTES_PER_WORKER', '0').to_i
          if bytes == 0
              # Comment describing rationale for choosing default of 256MiB/worker is below.
              bytes = 256 * (2**20)
          end
          bytes
      end

      # thread defaults as per https://github.com/puma/puma#thread-pool
      def get_min_threads()
          ENV.fetch('PUMA_MIN_THREADS', '0').to_i
      end

      def get_max_threads()
          ENV.fetch('PUMA_MAX_THREADS', '16').to_i
      end

      # Determine the maximum number of workers that are allowed by the available
      # memory.  Puma documentation recommends the maximum number of workers to be
      # set to the number of cores.
      # Unless we're specifically tuned otherwise, allow one worker process per 256MiB
      # memory, to a maximum of 1 worker / core.  Hopefully that'll be a reasonable
      # starting point for average apps; if not, it's all tunable.  The simple
      # OpenShift ruby/rails sample app currently requires approx. 60MiB +
      # 70MiB/worker before taking its first request, so hopefully a default of
      # 256MiB/worker will give other simple apps reasonable default headroom.
      def get_workers()
          return ENV['PUMA_WORKERS'].to_i if ENV.has_key? 'PUMA_WORKERS'

          cores = ENV.fetch('NUMBER_OF_CORES', '1').to_i
          max_workers = get_max_memory() / get_memory_per_worker()

          [cores, max_workers].min
      end

      environment ENV['RACK_ENV'] || ENV['RAILS_ENV'] || 'production'
      threads     get_min_threads(), get_max_threads()
      workers     get_workers()

      application_path="/discourse"
      directory application_path
      environment 'production'
      daemonize false
      pidfile "#{application_path}/tmp/pids/puma.pid"
      state_path "#{application_path}/tmp/pids/puma.state"
      preload_app!
      bind "tcp://0.0.0.0:8080"

    sidekiq.yml: |2
      :concurrency: 5
      :pidfile: /discourse/tmp/pids/sidekiq.pid
      production:
        :queues:
          - [critical,4]
          - [default, 2]
          - [low]

parameters:

  - name: DISCOURSE_DB_HOST
    description: "Database host for Discourse."
    required: true

  - name: DISCOURSE_DB_PORT
    description: "Database port for Discourse."
    required: true

  - name: DISCOURSE_DB_NAME
    description: "Database name for Discourse."
    value: discourse
    required: true

  - name: DISCOURSE_DB_USERNAME
    description: "Username for Discourse database."
    value: discourse
    required: true

  - name: DISCOURSE_DB_PASSWORD
    description: "Password for Discourse database."
    required: true

  - name: DISCOURSE_DEVELOPER_EMAILS
    description: "Comma-separated list of developer e-mails"
    required: true